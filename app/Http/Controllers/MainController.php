<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Product;
use Illuminate\Http\Request;

class MainController extends Controller
{
    //
    public function index()
    {
        $products = Product::orderByDesc('id')->get();
        $carts = Cart::with('products')->where('user_id',1)->get();

        return view('cart',compact('products','carts'));
    }
    public function addToCart(Request $request)
    {

        $product = Product::with('carts')->find($request->product_id);

        if($product->carts()->where('product_id',$product->id)->exists())
        {
            $cart =  Cart::where('user_id',1)->first();
          
           $update = $cart->products()->where('product_id',$request->product_id)->first();

           $update->pivot->update([
                'quantity' => $request->quantity
            ]);
            echo "cart updated";
        }
        else
        {
            $cart=Cart::create([
                'user_id'=>1,
            ]);
            $product->carts()->attach($cart,['product_id'=>$request->product_id,'quantity'=>$request->quantity]);
            echo "cart added";
        }

    }
}

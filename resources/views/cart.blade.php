<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <h1>Add to Cart</h1>

        <div class="row">
                @foreach($products as $key => $value)

        <form action="{{route('add.cart')}}" method="post">
            @csrf
                <div class="col-md-3" style="padding: 20px;">
                <div class="card">
                <div class="card" style="width: 18rem; ">
                    <img class="card-img-top" src="..." alt="Card image cap">
                    <div class="card-body">
                    <h5 class="card-title">{{$value->title}}</h5>
                    <p class="card-text">{{$value->descripion}}</p>
                    <input type="number" value="" name="quantity">
                    <input type="hidden" name="product_id" value="{{$value->id}}">
                    <button type="submit" class="btn btn-primary">Add to cart</a>
                    </div>
                  </div>
                </div>
            </div>
        </form>
                @endforeach




        </div>

        <div>
            <h1>Carts</h1>
            @foreach($carts as $key => $value)

            <form action="{{route('add.cart')}}" method="post">
                @csrf
                    <div class="col-md-3" style="padding: 20px;">
                    <div class="card">
                    <div class="card" style="width: 18rem; ">
                        <img class="card-img-top" src="..." alt="Card image cap">
                        <div class="card-body">
                        <h5 class="card-title">{{$value->title}}</h5>
                        <p class="card-text">{{$value->descripion}}</p>

                        <input type="number" value="{{$value->products->first()->pivot->quantity}}" name="quantity">
                        <input type="hidden" name="cart_id" value="{{$value->id}}">

                        <input type="hidden" name="product_id" value="{{$value->products->first()->id}}">
                        <button type="submit" class="btn btn-primary">Update cart</a>
                        </div>
                      </div>
                    </div>
                </div>
            </form>
             @endforeach
        </div>
    </div>
</body>
</html>

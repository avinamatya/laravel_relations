<?php

use App\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Product::create([
            'title'=>Str::random(6),
            'description' => Str::random(15)
        ]);
        Product::create([
            'title'=>Str::random(6),
            'description' => Str::random(15)
        ]);
        Product::create([
            'title'=>Str::random(6),
            'description' => Str::random(15)
        ]);
        Product::create([
            'title'=>Str::random(6),
            'description' => Str::random(15)
        ]);
        Product::create([
            'title'=>Str::random(6),
            'description' => Str::random(15)
        ]);
        
    }
}
